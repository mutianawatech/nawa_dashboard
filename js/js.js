 var buyerData = {
  labels : ["2008","2009","2010","2011","2012","2013"],
  datasets : [
  {
      fillColor : "transparent",
      strokeColor : "#FF821C",
      pointColor : "#fff",
      pointStrokeColor : "#F7CA18",
      data : [203,156,99,251,305,247]
  }
]
}
// get line chart canvas
var buyers = document.getElementById('buyers').getContext('2d');
// draw line chart
new Chart(buyers).Line(buyerData);

var barData = {
  labels : ["January","February","March","April","May","June"],
  datasets : [
      {
          fillColor : "#FF821C",
          data : [456,479,324,569,702,600]
      },
      {
          fillColor : "rgba(247, 202, 24, 0.24)",
          strokeColor : "rgba(247, 202, 24, 0.24)",
          data : [364,504,605,400,345,320]
      }
  ]
}
// get bar chart canvas
var income = document.getElementById("income").getContext("2d");
// draw bar chart
new Chart(income).Bar(barData);
var pieOptions = {
  segmentShowStroke : false,
  animateScale : true
}
// get pie chart canvas
var countries= document.getElementById("countries").getContext("2d");
// draw pie chart
new Chart(countries).Pie(pieData, pieOptions);
var pieData = [
  {
      value: 20,
      color:"#878BB6"
  },
  {
      value : 40,
      color : "#4ACAB4"
  },
  {
      value : 10,
      color : "#FF8153"
  },
  {
      value : 30,
      color : "#FFEA88"
  }
];
// pie chart options
var pieOptions = {
   segmentShowStroke : false,
   animateScale : true
}
// get pie chart canvas
var countries= document.getElementById("countries").getContext("2d");
// draw pie chart
new Chart(countries).Pie(pieData, pieOptions);




